extern crate chrono;
extern crate atoi;

use self::chrono::NaiveDateTime;

pub struct Log {
	pub dt: NaiveDateTime
}

fn extract_dt(line: Vec<&str>) -> NaiveDateTime {
	let dt: NaiveDateTime = NaiveDateTime::parse_from_str(line[3], "[%d/%b/%Y:%H:%M:%S").unwrap();
	return dt;
}

pub fn parse(line: String) -> Log {
	//very simple extract of DateTime for a success log
	let split = line.split(" ");
	let v: Vec<&str> = split.collect();
	let ret = Log {dt: extract_dt(v)};
	ret
}
extern crate time;
extern crate queues;

use queues::*;
use self::time::{Duration, PreciseTime};
use std::io::{BufRead, BufReader};
use std::fs::File;
use std::io::{self, Read};

mod parser;

fn main() {
	let mut filename = String::new();
    println!("Hi please give me the name of your log file.");
    io::stdin().read_to_string(&mut filename);

    // Test parse method
    let start = PreciseTime::now();
    let log = parser::parse(String::from("127.0.0.1 - - [28/Jul/2006:10:22:04 -0300] \"GET / HTTP/1.0\" 200 2216"));
    println!("{:?}", log.dt);
    println!("It takes {:?} to ", start.to(PreciseTime::now()));

    monitor_loop(&filename);
}

fn check_queue(q: &Queue<&parser::Log>, limit: Duration) {
	// Comparison between queue peek datetime and now.
	// Remove every log not contain in the duration.
	
	// Type trouble ...
	while q.size() > 0 {
		let peek = q.peek().expect("How it is possible ?");
		if peek.dt.to(/*Something like now ?*/) > limit {
			q.remove();
		} else {
			break;
		}
	}
}

fn monitor_loop(filename: &String) {
	// This function read the file filename and print the number of log during the last five sec, and the last minute.
	let mut five_sec_queue: Queue<&parser::Log> = queue![];
	let five_sec = Duration::seconds(5);
	let mut one_min_queue: Queue<&parser::Log> = queue![];
	let one_min = Duration::minutes(1);

	let f = File::open(filename).expect("file not found");
	let mut reader = BufReader::new(f);
	let mut line = String::new();

	loop {
		let len = reader.read_line(&mut line).expect("Oupsy...");
		if len > 0 {
			let log = parser::parse(line);

			five_sec_queue.add(&log);
			one_min_queue.add(&log);

			check_queue(&mut five_sec_queue, five_sec);
			check_queue(&mut one_min_queue, one_min);

		}
		println!("{} log during the last 5sec --- {} log during the last minute", 
			five_sec_queue.size(), one_min_queue.size());
	}
}